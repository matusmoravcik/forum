/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.dao.impl;

import com.mycompany.forum.Application;
import com.mycompany.forum.entity.Thread;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author Matus
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest//("server.port:0")
@Transactional
public class ThreadManagerImplTest {    
    @Autowired
    private ThreadManagerImpl threadManager;
    
    private Thread dummyThread;
    
    public ThreadManagerImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        dummyThread = new Thread();
        dummyThread.setName("dummy thread");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createThread method, of class ThreadManagerImpl.
     */
    @Test
    public void testCreateThread() {
        System.out.println("createThread");
        
        //count existing threads first
        List<Thread> threads = threadManager.getAllThreads();
        int threadCount = threads.size();
        
        //create a new thread
        threadManager.createThread(dummyThread);
        
        //count existing threads again
        threads = threadManager.getAllThreads();
        assert(threads.size() == threadCount + 1): "Number of threads after creation doesn't add up.";
        
        //check existance of the newly created thread
        assert(threads.contains(dummyThread)): "Cannot find the created thread.";        
    }

    /**
     * Test of readThread method, of class ThreadManagerImpl.
     */
    @Test
    public void testReadThread() {
        System.out.println("readThread");
        
        //try reading non-existant thread
        Thread result = threadManager.readThread(1);
        assert(result == null): "No thread should exist.";
        
        //create dummy thread
        threadManager.createThread(dummyThread);
        
        //read thread
        result = threadManager.readThread(dummyThread.getId());        
        assert(dummyThread.equals(result)): "Cannot find the last created thread.";
    }

    /**
     * Test of updateThread method, of class ThreadManagerImpl.
     */    
    @Test
    public void testUpdateThread() {
        System.out.println("updateThread");
        
        //create a new thread
        threadManager.createThread(dummyThread);
        
        //modify the thread and update database
        dummyThread.setName("updated dummy thread");
        threadManager.updateThread(dummyThread);
        
        //check for modifications
        Thread retrievedThread = threadManager.readThread(dummyThread.getId());        
        assert(dummyThread.equals(retrievedThread)): "Cannot find the updated thread.";
    }

    /**
     * Test of deleteThread method, of class ThreadManagerImpl.
     */
    @Ignore
    @Test
    public void testDeleteThread() {
        System.out.println("deleteThread");
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllThreads method, of class ThreadManagerImpl.
     */
    @Test
    public void testGetAllThreads() {
        System.out.println("getAllThreads");       
        
        //try reading threads when there are none
        List<Thread> threads = threadManager.getAllThreads();
        assert(threads.isEmpty()): "No threads should exist.";
        
        //create a new dummy thread
        threadManager.createThread(dummyThread);
        
        //check for created thread
        threads = threadManager.getAllThreads();
        assert(threads.contains(dummyThread)): "Cannot find the last created thread.";
        
        //create a second dummy thread
        Thread t2 = new Thread();
        t2.setName("dummy thread 2");
        threadManager.createThread(t2);
        
        //check if more than 1 threads are retrieved
        threads = threadManager.getAllThreads();
        assert(threads.size() == 2): "Two threads should exist.";  
        assert(threads.contains(t2)): "Cannot find the last created thread.";
    }
}
