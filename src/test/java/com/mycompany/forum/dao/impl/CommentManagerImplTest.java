/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.dao.impl;

import com.mycompany.forum.Application;
import com.mycompany.forum.entity.Comment;
import com.mycompany.forum.entity.Thread;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author Matus
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class CommentManagerImplTest {
    @Autowired
    private ThreadManagerImpl threadManager;
    
    private Thread dummyThread;
    private Comment dummyComment;
    
    @Autowired
    private CommentManagerImpl commentManager;
    
    public CommentManagerImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {   
        Thread thread = new Thread();
        thread.setName("dummy thread");
        threadManager.createThread(thread);
        List<Thread> threads = threadManager.getAllThreads();
        dummyThread = threads.get(0);
        
        dummyComment= new Comment();
        dummyComment.setThread(dummyThread);
        dummyComment.setText("dummy comment");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createComment method, of class CommentManagerImpl.
     */
    @Test
    public void testCreateComment() {
        System.out.println("createComment");
        
        //count existing comments first
        List<Comment> comments = commentManager.getAllComments();
        int commentCount = comments.size();
        
        //create a new comment
        commentManager.createComment(dummyComment);
        
        //count existing comments again
        comments = commentManager.getAllComments();
        assert(comments.size() == commentCount + 1): "Number of comments after creation doesn't add up.";
        
        //check existance of the newly created comment
        assert(comments.contains(dummyComment)): "Cannot find the created comment.";
    }

    /**
     * Test of readComment method, of class CommentManagerImpl.
     */
    @Test
    public void testReadComment() {
        System.out.println("readComment");        
        
        Comment result = commentManager.readComment(1);
        assert(result == null): "No comment should exist.";
        
        commentManager.createComment(dummyComment);
        
        result = commentManager.readComment(dummyComment.getId());        
        assert(dummyComment.equals(result)): "Cannot find the last created comment.";
    }

    /**
     * Test of updateComment method, of class CommentManagerImpl.
     */
    @Test
    public void testUpdateComment() {
        System.out.println("updateComment");
        
        //create a new comment
        commentManager.createComment(dummyComment);
        
        //modify the comment and update database
        dummyComment.setText("updated dummy comment");
        commentManager.updateComment(dummyComment);
        
        //check for modifications
        Comment retrievedComment = commentManager.readComment(dummyComment.getId());        
        assert(dummyComment.equals(retrievedComment)): "Cannot find the last updated comment.";
    }

    /**
     * Test of deleteComment method, of class CommentManagerImpl.
     */
    @Ignore
    @Test
    public void testDeleteComment() {
        System.out.println("deleteComment");
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAllThreadComments method, of class CommentManagerImpl.
     */
    @Test
    public void testGetAllThreadComments() {
        System.out.println("getAllThreadComments");
        
        //try reading comments when there are none
        List<Comment> comments = commentManager.getAllThreadComments(dummyThread.getId());
        assert(comments.isEmpty()): "No comments should exist.";
        
        //create a new dummy comment
        commentManager.createComment(dummyComment);
        dummyThread.getComments().add(dummyComment);
        
        //check for created comment
        comments = commentManager.getAllThreadComments(dummyThread.getId());
        assert(comments.contains(dummyComment)): "Cannot find the last created comment.";
        
        //create a second dummy comment
        Comment c2 = new Comment();
        c2.setText("dummy comment 2");
        c2.setThread(dummyThread);
        commentManager.createComment(c2);
        dummyThread.getComments().add(c2);
        
        //check if more than 1 comments are retrieved
        comments = commentManager.getAllThreadComments(dummyThread.getId());
        assert(comments.size() == 2): "Two comments should exist.";  
        assert(comments.contains(c2)): "Cannot find the last created comment.";
    }
    
    /**
     * Test of getAllComments method, of class CommentManagerImpl.
     */
    @Test
    public void testGetAllComments() {
        System.out.println("getAllComments");
        
        //try reading comments when there are none
        List<Comment> comments = commentManager.getAllComments();
        assert(comments.isEmpty()): "No comments should exist.";
        
        //create a new dummy comment
        commentManager.createComment(dummyComment);
        
        //check for created comment
        comments = commentManager.getAllComments();
        assert(comments.contains(dummyComment)): "Cannot find the last created comment.";
        
        //create a second dummy comment
        Comment c2 = new Comment();
        c2.setText("dummy comment 2");
        c2.setThread(dummyThread);
        commentManager.createComment(c2);
        
        //check if more than 1 comments are retrieved
        comments = commentManager.getAllComments();
        assert(comments.size() == 2): "Two comments should exist.";  
        assert(comments.contains(c2)): "Cannot find the last created comment.";
    }
}
