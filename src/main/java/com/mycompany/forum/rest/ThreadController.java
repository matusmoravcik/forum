/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.rest;

import com.mycompany.forum.dto.DateFormat;
import com.mycompany.forum.dto.ThreadDTO;
import com.mycompany.forum.service.ThreadService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Matus
 */
@RestController
@RequestMapping("/thread")
public class ThreadController {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DateFormat.FORMAT);

    @Autowired
    private ThreadService service; 
    
    public void setService(ThreadService service) {
        this.service = service;
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public @ResponseBody ThreadDTO postThread(@RequestParam(value="name") String name)
    {
        ThreadDTO threadDTO = new ThreadDTO();
        threadDTO.setName(name);
        return service.createThread(threadDTO);        
    }
    
    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody List<ThreadDTO> getThreads()
    {
        return service.getThreads();
    }
    
    @RequestMapping(value = "{id}", method=RequestMethod.PUT)
    public @ResponseBody ThreadDTO putThread(
        @PathVariable("id") long id,
        @RequestParam(value="name") String name,
        @RequestParam(value="created") String created) throws ParseException
    {
        ThreadDTO threadDTO = new ThreadDTO();
        threadDTO.setId(id);
        threadDTO.setName(name);        
        threadDTO.setCreated(DATE_FORMAT.parse(created));
        service.updateThread(threadDTO);
        return threadDTO;
    }
}
