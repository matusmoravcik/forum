/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mycompany.forum.dto.CommentDTO;
import com.mycompany.forum.dto.DateFormat;
import com.mycompany.forum.service.CommentService;
import com.mycompany.forum.service.ThreadService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Matus
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DateFormat.FORMAT);
    
    @Autowired
    private CommentService commentService;
    
    @Autowired
    private ThreadService threadService;
    
    public void setService(CommentService commentService) {
        this.commentService = commentService;
    }

    public void setThreadService(ThreadService threadService) {
        this.threadService = threadService;
    }
    
    @Transactional
    @RequestMapping(method=RequestMethod.POST)
    public @ResponseBody CommentDTO postComment(
        @RequestParam(value="threadId") long threadId,
        @RequestParam(value="description") String description)
    {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setThread(threadService.getThread(threadId));
        commentDTO.setText(description);
        return commentService.createComment(commentDTO);
    }
    
    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody List<CommentDTO> getComments(@RequestParam(value="threadId") long threadId)
    {
        return commentService.getThreadComments(threadId);
    }
    
    @Transactional
    @RequestMapping(value = "{id}", method=RequestMethod.PUT)
    public @ResponseBody CommentDTO putComment(
        @PathVariable("id") long id,
        @RequestParam(value="threadId") long threadId,
        @RequestParam(value="description") String description,
        @RequestParam(value="created") String created) throws ParseException
    {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(id);
        commentDTO.setThread(threadService.getThread(threadId));
        commentDTO.setText(description);
        commentDTO.setCreated(DATE_FORMAT.parse(created));
        commentService.updateComment(commentDTO);
        return commentDTO;
    }
}
