/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.service;

import com.mycompany.forum.dto.CommentDTO;
import java.util.List;

/**
 *
 * @author Matus
 */
public interface CommentService {
    /**
     * Returns comments belonging to a certain thread.
     * @param threadId ID of a thread
     * @return thread comments
     */
    public List<CommentDTO> getThreadComments(long threadId);  
    
    /**
     * @param commentDTO comment to create
     * @return created comment
     */
    public CommentDTO createComment(CommentDTO commentDTO);   
    
    /**
     * @param commentDTO updated comment
     */
    public void updateComment(CommentDTO commentDTO);
    
    /**
     * @param id ID of a comment to delete
     */
    public void deleteComment(long id);
}
