/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.service.impl;

import com.mycompany.forum.dao.ThreadManager;
import com.mycompany.forum.dto.ThreadDTO;
import com.mycompany.forum.service.ThreadService;
import com.mycompany.forum.entity.Thread;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Matus
 */
@Service
public class ThreadServiceImpl implements ThreadService {
    @Autowired
    private ThreadManager manager;
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
    
    public void setManager(ThreadManager manager) {
        this.manager = manager;
    }
    
    @Override    
    public ThreadDTO getThread(long id)
    {
        return mapper.map(manager.readThread(id), ThreadDTO.class);
    }

    @Override
    @Transactional()
    public List<ThreadDTO> getThreads()
    {
        List<ThreadDTO> result = new ArrayList<>();
        for (Thread thread : manager.getAllThreads())
        {
            result.add(mapper.map(thread, ThreadDTO.class));
        }
        return result; 
    }

    @Override
    @Transactional()
    public ThreadDTO createThread(ThreadDTO threadDTO)
    {
        threadDTO.setCreated(new Date());
        Thread thread = mapper.map(threadDTO, Thread.class);
        manager.createThread(thread);
        return mapper.map(thread, ThreadDTO.class);
    }

    @Override
    @Transactional()
    public void updateThread(ThreadDTO threadDTO) 
    {
        manager.updateThread(mapper.map(threadDTO, Thread.class));        
    }

    @Override
    @Transactional()
    public void deleteThread(long id) 
    {
        manager.deleteThread(id);
    }
}
