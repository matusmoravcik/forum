/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.service.impl;

import com.mycompany.forum.dao.CommentManager;
import com.mycompany.forum.dto.CommentDTO;
import com.mycompany.forum.service.CommentService;
import com.mycompany.forum.entity.Comment;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Matus
 */
@Service
public class CommentServiceImpl implements CommentService{
    @Autowired
    private CommentManager manager;    
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

    public void setManager(CommentManager manager) {
        this.manager = manager;
    }

    @Override
    @Transactional()
    public List<CommentDTO> getThreadComments(long threadId)
    {
        List<CommentDTO> result = new ArrayList<>();
        for (Comment comment : manager.getAllThreadComments(threadId))
        {
            result.add(mapper.map(comment, CommentDTO.class));
        }
        return result;        
    }

    @Override
    @Transactional()
    public CommentDTO createComment(CommentDTO commentDTO)
    {
        commentDTO.setCreated(new Date());
        Comment comment = mapper.map(commentDTO, Comment.class);        
        manager.createComment(comment);
        return mapper.map(comment, CommentDTO.class);
    }

    @Override
    @Transactional()
    public void updateComment(CommentDTO commentDTO)
    {
        manager.updateComment(mapper.map(commentDTO, Comment.class));        
    }

    @Override
    @Transactional()
    public void deleteComment(long id)
    {
        manager.deleteComment(id);
    }
}
