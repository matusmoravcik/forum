/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.service;

import com.mycompany.forum.dto.ThreadDTO;
import java.util.List;

/**
 *
 * @author Matus
 */
public interface ThreadService {
    /**
     * 
     * @param id ID of a thread to retrieve
     * @return retrieved thread
     */
    public ThreadDTO getThread(long id);
    
    /**
     * 
     * @return list of all threads
     */
    public List<ThreadDTO> getThreads();    
    
    /**
     * 
     * @param threadDTO thread to create
     * @return created thread
     */
    public ThreadDTO createThread(ThreadDTO threadDTO);   
    
    /**
     * 
     * @param threadDTO updated thread
     */
    public void updateThread(ThreadDTO threadDTO); 
    
    /**
     * 
     * @param id ID of a thread to delete
     */
    public void deleteThread(long id);
}
