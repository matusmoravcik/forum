/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.dao.impl;

import com.mycompany.forum.dao.CommentManager;
import com.mycompany.forum.entity.Comment;
import com.mycompany.forum.entity.Thread;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Matus
 */

@Repository
public class CommentManagerImpl implements CommentManager {
    @PersistenceContext
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public void createComment(Comment comment)
    {
        em.persist(comment);
        em.close();
    }

    @Override
    public Comment readComment(long id)
    {
        Comment result = em.find(Comment.class, id);
        em.close();
        return result;
    }

    @Override
    public void updateComment(Comment comment)
    {
        em.merge(comment);  
        em.close();
    }

    @Override    
    public void deleteComment(long id)
    {
        Comment comment = em.find(Comment.class, id);
        if (comment != null) {
            em.remove(comment); 
        }
        em.close();
    }
    
    @Override
    public List<Comment> getAllThreadComments(long threadId)
    {
        List<Comment> result = em.find(Thread.class, threadId).getComments();
        em.close();
        return result;
    }
    
    @Override   
    public List<Comment> getAllComments()
    {
        List<Comment> result = em.createQuery("SELECT c from Comment c", Comment.class).getResultList();  
        em.close();
        return result;
    }
}
