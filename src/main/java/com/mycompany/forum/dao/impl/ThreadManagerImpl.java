/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.dao.impl;

import com.mycompany.forum.dao.ThreadManager;
import com.mycompany.forum.entity.Thread;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Matus
 */

@Repository
public class ThreadManagerImpl implements ThreadManager {
    @PersistenceContext
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public void createThread(Thread thread)
    {
        em.persist(thread);
        em.close();        
    }

    @Override
    public Thread readThread(long id)
    {
        Thread result = em.find(Thread.class, id);
        em.close();
        return result;
    }

    @Override
    public void updateThread(Thread thread)
    {
        em.merge(thread);  
        em.close();
    }

    @Override
    public void deleteThread(long id)
    {
        Thread thread = em.find(Thread.class, id);
        if (thread != null) {
            em.remove(thread); 
        }
        em.close();
    }
    
    @Override
    public List<Thread> getAllThreads()
    {
        List<Thread> result = em.createQuery("SELECT t from Thread t", Thread.class).getResultList();        
        em.close();
        return result;
    }
}
