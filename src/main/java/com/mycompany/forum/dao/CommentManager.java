/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.dao;

import com.mycompany.forum.entity.Comment;
import java.util.List;

/**
 *
 * @author Matus
 */
public interface CommentManager {
    /**
     * 
     * @param comment comment to persist
     */
    public void createComment(Comment comment); 
    
    /**
     * 
     * @param id ID of a comment to read
     * @return retrieved comment
     */
    public Comment readComment(long id);
    
    /**
     * Updates the comment with the same ID with new parameters.
     * @param comment comment to update
     */
    public void updateComment(Comment comment);
    
    /**
     * 
     * @param id ID of a comment to remove
     */
    public void deleteComment(long id);
    
    /**
     * 
     * @param threadId ID of a thread of which comments to retrieve
     * @return list of comments of a certain thread
     */
    public List<Comment> getAllThreadComments(long threadId);
    
    /**
     * 
     * @return all persisted comments
     */
    public List<Comment> getAllComments();
}
