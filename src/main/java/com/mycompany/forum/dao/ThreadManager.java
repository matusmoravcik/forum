/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.forum.dao;

import com.mycompany.forum.entity.Thread;
import java.util.List;

/**
 *
 * @author Matus
 */
public interface ThreadManager {
    /**
     * 
     * @param thread thread to create
     */
    public void createThread(Thread thread);  
    
    /**
     * 
     * @param id ID of a thread to read
     * @return retrieved thread
     */
    public Thread readThread(long id);
    
    /**
     * Updates the thread with the same ID with new parameters.
     * @param thread thread to update
     */
    public void updateThread(Thread thread);
    
    /**
     * 
     * @param id ID of a thread to delete
     */
    public void deleteThread(long id);
    
    /**
     * 
     * @return all persisted threads
     */
    public List<Thread> getAllThreads();
}
